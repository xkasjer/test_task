<?php

class IndexController extends Zend_Controller_Action {

  public function init() {
    $this->view->login = $this->login = new Login($this->getRequest());
    if ($this->login->tryLogin()) {
      $this->view->user = $this->user = $this->login->getUser();
    }
    }

    public function indexAction(){
    $this->view->headScript()->appendFile('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js')
         ->appendFile('/assets/js/validate.js');
    $id_gallery = $this->_request->getParam('gallery_id');
    $this->view->gallery = Gallery::getGallery($id_gallery);
    $this->view->gallery_name = Gallery::getGalleryName($id_gallery);
  }

  public function panelAction() {
    if (!$this->login->tryLogin()) {
      $this->_redirect("/");
    }
    $gallery = new Gallery($this->user->getId());
    $this->view->gallery_private = $gallery->isPrivateGallery();
    $this->view->gallery = $gallery->getMyGallery();
  }

  public function usunImageAction() {
    if (!$this->login->tryLogin()) {
      $this->_redirect("/");
    }
    $id_image = $this->_request->getParam('image_id');
    $gallery = new Gallery($this->user->getId());
    $name_image = $gallery->deleteImage($id_image);
    echo $name_image;
    unlink('img/' . $name_image);
    unlink('img/thumb/' . $name_image);
    $this->_redirect("/index/panel");
    $this->_helper->viewRenderer->setNoRender();
  }

  public function changePrivateAction() {
    if (!$this->login->tryLogin()) {
      $this->_redirect("/");
    }
    $gallery = new Gallery($this->user->getId());
    $gallery->changePrivate();
    $this->_redirect("/index/panel");
    $this->_helper->viewRenderer->setNoRender();
  }

  public function uploadImgGaleriaAction() {
    if (!$this->login->tryLogin()) {
      $this->_redirect("/");
    }
    require_once "Images/Images.php";
    $gallery = new Gallery($this->user->getId());
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();
    $return = Images::uploadImg($_FILES['zdjecie'], 'images', 'img/', array('x' => 0, 'y' => 50));
    if ($return['path']) {
      $gallery->addImage($return['path']);
    }
    $this->_redirect("/index/panel");
  }

}


