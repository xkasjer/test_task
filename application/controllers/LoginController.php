<?php

class LoginController extends Zend_Controller_Action {

  public $panelSession;

  public function init() {
    require_once "Db/Db_Db.php";
    try {
      $this->db = Db_Db::conn();
    } catch (Zend_Db_Exception $e) {
      echo $e->getMessage();
    }

    //autoryzacja
    require_once "Login/Login.php";
  }

  public function logoutAction() {
    $login = new Login($this->getRequest());
    $login->logout();
    $this->_redirect("/");
    $this->_helper->viewRenderer->setNoRender();
  }

  public function loginCheckAction() {
    $check = User::checkLogin($this->_request->getParam('login'));
    if ($check) {
      echo "false";
    } else {
      echo "true";
    }
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();
  }

  public function signUpAction() {
    $check = User::checkLogin($this->_request->getParam('login'));
    if (!$check) {
      if ($this->_request->getParam('password') == $this->_request->getParam('password_confirm') and strlen($this->_request->getParam('password')) > 5) {
        User::addAcount($this->_request->getParam('login'), $this->_request->getParam('password'));
      }
    }
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();
    $this->_redirect("/");
  }

}
