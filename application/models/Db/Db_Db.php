<?php

/**
 * Database handler.
 *
 */
class Db_Db {

  public static function conn() {
    $connParams = array("host" => "",
       "port" => "",
       "username" => "",
       "password" => "",
       "dbname" => "");
    try {

      $db = new Zend_Db_Adapter_Pdo_Mysql($connParams);
      $db->query("SET character_set_connection=utf8");
      $db->query("SET character_set_client=utf8");
      $db->query("SET character_set_results=utf8");
      $db->query("SET lc_time_names = 'pl_PL'");
    } catch (Zend_Db_Exception $e) {
      echo $e->getMessage();
    }

    return $db;
  }

  public static function isNameFree($nazwaObr, $table, $table_field) {

    $db = Db_Db::conn();

    do {

      $tmpSalt = "";
      $tmpSalt.= rand(0, 9);
      $nazwaObr = $tmpSalt . $nazwaObr;

      $inne = "SELECT 1 FROM `" . $table . "` WHERE `" . $table_field . "` = '" . $nazwaObr . "'";
      try {

        $result_inne = $db->fetchAll($inne);
      } catch (Zend_Db_Exception $e) {
        echo $e->getMessage();
      }
    } while (count($result_inne) > 0);


    return($nazwaObr);
  }

}

?>
