<?php

class Gallery extends User {

  public function __construct($idUser) {
    parent::__construct(NULL, $idUser);
  }

  public function getMyGallery() {
    $sql = $this->db->select()->from('images', array('name_images', 'idimages'))->where('idusers=?', $this->getId());
    $result = $this->db->fetchAll($sql);
    return $result;
  }

  public function deleteImage($id_image) {
    $sql = $this->db->select()->from('images', array('name_images'))
              ->where('idusers=?', $this->getId())->where('idimages=?', $id_image);
    $name = $this->db->fetchOne($sql);
    $this->db->delete('images', array('idusers=?' => $this->getId(), 'idimages=?' => $id_image));
    return $name;
  }

  public function addImage($name_images) {
    $count = $this->db->insert('images', array('name_images' => $name_images, 'idusers' => $this->getId()));
    return $count;
  }

  public function isPrivateGallery() {
    $sql = $this->db->select()->from('users', array('private'))
         ->where('idusers=?', $this->getId());
    return $this->db->fetchOne($sql);
  }

  public function changePrivate() {
    $private = $this->isPrivateGallery();
    $private = ($private == 1) ? 0 : 1;
    $count = $this->db->update('users', array('private' => $private), array('idusers=?' => $this->getId()));
    return $count;
  }

  public static function getAllGalleries($idusers) {
    $db = Db_Db::conn();
    $sql = "SELECT idusers,login FROM `users` where private=0 ";
    if ($idusers != null)
      $sql.="or idusers=" . $db->quote($idusers);
    
    $result = $db->fetchAll($sql);
    return $result;
  }

  public static function getGalleryName($id) {
    $db = Db_Db::conn();
    if ($id == NULL) {
      $id = Gallery::getFirstGallery();
    }
    $sql = $db->select()->from('users', array('login'))->where('idusers=?', $id);
    return $db->fetchOne($sql);
  }

  public static function getGallery($id) {
    $db = Db_Db::conn();
    if ($id == NULL) {
      $id = Gallery::getFirstGallery();
    }

    $sql = $db->select()->from('images', array('name_images'))->where('idusers=?', $id);
    $result = $db->fetchAll($sql);
    return $result;
  }

  public static function getFirstGallery() {
    $db = Db_Db::conn();
    $sql = $db->select()->from('users', array('idusers'))->limit(1);
    return $db->fetchOne($sql);
  }

}
