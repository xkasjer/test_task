<?php

class User {

  protected $db = "";
  protected $id;
  protected $login;
  private $main_table = "users";

  public function __construct($login, $idUser = null) {
    $this->db = Db_Db::conn();
    $this->login = $login;
    $this->id = $idUser;


    if ($login == null) {

      $login = "SELECT `login` FROM $this->main_table WHERE `idusers` = '" . $this->id . "'";

      $this->login = $this->db->fetchOne($login);

      if ($this->login) {
        return new User($this->login, $this->db);
      } else {
        throw new UserException("Invalid login adress ", "Podany adres login nie został znaleziony!");
      }
    } else {
      $login = "SELECT `login` FROM $this->main_table WHERE `login` = '" . $this->login . "'";
      $this->login = $this->db->fetchOne($login);
      if (!$this->login) {
        throw new UserException("Invalid login adress ", "Podany adres login nie został znaleziony!");
      }
    }
  }

  public function getLogin() {
    return $this->login;
  }

  public function getId() {
    if ($this->id) {
      return $this->id;
    } else {
      try {
        $id = "SELECT `idusers` FROM $this->main_table WHERE `login` = '" . $this->login . "'";
        $this->id = $this->db->fetchOne($id);
        return $this->id;
      } catch (Zend_Db_Exception $DbE) {
        echo $DbE->getMessage();
      }
    }
  }

  public static function checkLogin($login) {
    try {
      $db = Db_Db::conn();
      $sql = $db->select()->from('users')->where('login=?', $login);
      $result = $db->fetchAll($sql);
      if (count($result))
        return true;
      return false;
    } catch (Zend_Db_Exception $DbE) {
      echo $DbE->getMessage();
    }
  }

  public static function addAcount($login, $pass) {
    try {
      $db = Db_Db::conn();
      $sql = $db->insert('users', array('login' => $login, 'password' => md5($pass)));
    } catch (Zend_Db_Exception $DbE) {
      echo $DbE->getMessage();
    }
  }

}

class UserException extends Exception {

  private $wrongData;

  public function __construct($message = null, $wrongData, $code = 0) {
    $this->wrongData = $wrongData;
    $t_message = "Exception raised in User ";
    $t_message .= "with message : " . $message;
    $t_message .= " on line : " . $this->getLine();

    parent::__construct($t_message, $code);
  }

  public function getWrongData() {
    return $this->wrongData;
  }

}

?>
