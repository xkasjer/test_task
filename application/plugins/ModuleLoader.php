<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of test
 *
 * @author 2
 */
class Application_Plugin_ModuleLoader extends Zend_Controller_Plugin_Abstract{
    
    protected $modules;

    public function __construct()
    {
        $modules = Zend_Controller_Front::getInstance()->getControllerDirectory();
        $this->modules = $modules;
        
    }

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $module = $request->getModuleName();
 
        // moduł domyślny można ominąć
        if($module == 'default') {
            return;
        }
 
        // ścieżka do pliku Bootstrap
        $path = APPLICATION_PATH . '/modules/' . $module . '/Bootstrap.php';
 
        // nazwa klasy Bootstrap
        $class = ucfirst($module) . '_Bootstrap';
 
        // inicjalizacja, załadowanie i utworzenia obiektu klasy Bootstrap modułu
 
        $application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/modules/' . $module . '/configs/module.ini'
        );
 
        Zend_Loader::loadFile('Bootstrap.php', dirname($path));
        $bootstrap = new $class($application);
        $bootstrap->bootstrap();
    }
}

?>
