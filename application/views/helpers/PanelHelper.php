<?php

class Zend_View_Helper_PanelHelper extends Zend_View_Helper_Abstract {

  public function panelHelper($db, $login) {
    $iduser = null;
    if ($login) {
      $this->view->islogged = true;
      $iduser = $login->getId();
    }
    $this->view->galleries_helper = Gallery::getAllGalleries($iduser);
    if (isset($_GET['gallery_id'])) {
      $this->view->actualgallery = $_GET['gallery_id'];
    } else {
      $this->view->actualgallery = Gallery::getFirstGallery();
    }
    return $this->view->render('helpers/panel/panel.phtml');
  }

}

?>
