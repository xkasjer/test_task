$(document).ready(function() {

    $("#form_sign_up").validate({
        rules: {
            login: {
                required: true,
                minlength: 3,
                remote: {
                    url: "/login/login-check",
                    type: "post",
                }, },
            password: {
                required: true,
                minlength: 5
            },
            password_confirm: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
    messages: {
        login: {
            remote: " already exists"
            },

        },
    });

})